import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { UniqueIdUtil } from '@ngcore/core';
import { MediaType, MediaAttachment } from '@ngcore/base';
import { BaseModel, ExtendedModel } from '@ngcore/base';


/**
 * Represents a user profile.
 */
export class UserProfile extends ExtendedModel // extends BaseModel 
{
  // Since we only support a single profile/preference per user.
  // Using userId, not the id of the profile/pref, is better....
  // getDocId(): string {
  //   return UserProfile.name.toLowerCase() + ':' + this._userId;
  // }
  // setDocId(_id: string) {
  //   this._userId = (_id && _id.length > UserProfile.name.length + 1)
  //     ? _id.substr(UserProfile.name.length + 1) 
  //     : _id;  // ???
  // }
  // Just use userId as the doc id.
  static getDocId(userId: (string | null)): string {
    return (userId) ? userId : '';  // ???
  }
  getDocId(): string {
    // return (this.userId != null) ? this.userId : '';  // ???
    return this.userId;
  }
  setDocId(_id: string) {
    this.userId = _id;
  }


  // id: string = '0';
  // userId: string;
  // username: string;

  // TBD:
  // Photos/avatar images
  // ....


  // Here vs in AppUser ???
  public nickname: (string | null) = null;
  // getNickname(): string { return this.nickname; }
  setNickname(_nickname: string) { this.nickname = _nickname; this.isDirty = true; }

  public shortBio: (string | null) = null;
  // getShortBio(): string { return this.shortBio; }
  setShortBio(_shortBio: string) { this.shortBio = _shortBio; this.isDirty = true; }
  // ...


  // get userId(): string { return this._userId; }
  // set userId(_userId: string) { this._userId = _userId; this.isDirty = true; }

  // Note: We use RandomId instead of UniqueId for userId.
  //       This is necessary because userId can be used as Hash Key in DynamoDB.
  constructor(private _uid: (string | null) = null) {
    super(null, _uid);
  }


  // testing
  private static ATTACHMENT_NAME_PRIMARY_PHOTO: string = "_userprofile_primary_photo";
  storePrimaryPhoto(_data: any) {
    let attach = new MediaAttachment(null, this.userId);
    attach.name = UserProfile.ATTACHMENT_NAME_PRIMARY_PHOTO;
    attach.mediaType = MediaType.image;
    // attach.contentType = ???
    attach.data = _data;  // ???
    if (!this.attachments) this.attachments = {};
    this.attachments[UserProfile.ATTACHMENT_NAME_PRIMARY_PHOTO] = attach;
  }
  // fetchPrimaryPhoto(): any {
  //   let blob: any = null;
  //   if(this.hasImageAttachment(UserProfile.ATTACHMENT_NAME_PRIMARY_PHOTO)) {
  //     let attach = this.attachments[UserProfile.ATTACHMENT_NAME_PRIMARY_PHOTO];
  //     blob = attach.data;  // ???
  //   }
  //   // content type ???
  //   return blob;
  // }
  fetchPrimaryPhoto(): (MediaAttachment | null) {
    let attach: (MediaAttachment | null) = null;
    if(this.hasImageAttachment(UserProfile.ATTACHMENT_NAME_PRIMARY_PHOTO)) {
      attach = this.attachments[UserProfile.ATTACHMENT_NAME_PRIMARY_PHOTO];
    }
    return attach;
  }
  get hasPrimaryPhoto(): boolean {
    return (this.hasImageAttachment(UserProfile.ATTACHMENT_NAME_PRIMARY_PHOTO));
  }
  // testing


  toString(): string {
    return super.toString()
      + '; userId = ' + this.userId
      + '; nickname = ' + this.nickname
      + '; shortBio = ' + this.shortBio;
  }



  clone(): UserProfile {
    let cloned = Object.assign(new UserProfile(), this) as UserProfile;
    return cloned;
  }
  static clone(obj: any): UserProfile {
    let cloned = Object.assign(new UserProfile(), obj) as UserProfile;
    return cloned;
  }

  copy(): UserProfile {
    let obj = this.clone();
    // obj.id = RandomIdUtil.id();
    obj.id = UniqueIdUtil.id();
    // userId ????
    obj.resetCreatedTime();
    return obj;
  }
}
