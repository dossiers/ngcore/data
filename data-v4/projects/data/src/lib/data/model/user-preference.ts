import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { UniqueIdUtil, HourRange } from '@ngcore/core';
import { BaseModel } from '@ngcore/base';


/**
 * Represents a user preference.
 */
export class UserPreference extends BaseModel {

  // Since we only support a single profile/preference per user.
  // Using userId, not the id of the profile/pref, is better....
  // getDocId(): string {
  //   return UserPreference.name.toLowerCase() + ':' + this._userId;
  // }
  // setDocId(_id: string) {
  //   this._userId = (_id && _id.length > UserPreference.name.length + 1)
  //     ? _id.substr(UserPreference.name.length + 1)
  //     : _id;  // ???
  // }
  // Just use userId as the doc id.
  static getDocId(userId: (string | null)): string {
    return (userId) ? userId : '';  // ???
  }
  getDocId(): string {
    // return (this.userId != null) ? this.userId : '';  // ???
    return this.userId;
  }
  setDocId(_id: string) {
    this.userId = _id;
  }


  // id: string = '0';
  // userId: string;
  // username: string;




  // Default values?
  // type?
  public timezone: (string | null) = null;
  // getTimezone(): string { return this.timezone; }
  setTimezone(_timezone: string) { this.timezone = _timezone; this.isDirty = true; }


  // startHour: number; 
  // endHour: number;

  // e.g., [7AM to 2AM next day)
  public dayHours: HourRange;
  // getDayHours(): HourRange { return this.dayHours; }
  setDayHours(_dayHours: HourRange) { this.dayHours = _dayHours; this.isDirty = true; }

  // e.g., [9 to 5)
  public workHours: HourRange;
  // getWorkHours(): HourRange { return this.workHours; }
  setWorkHours(_workHours: HourRange) { this.workHours = _workHours; this.isDirty = true; }

  public amHours: HourRange;
  // getAmHours(): HourRange { return this.amHours; }
  setAmHours(_amHours: HourRange) { this.amHours = _amHours; this.isDirty = true; }

  public pmHours: HourRange;
  // getPmHours(): HourRange { return this.pmHours; }
  setPmHours(_pmHours: HourRange) { this.pmHours = _pmHours; this.isDirty = true; }

  public maxTasksPerDay: number = 0;
  // getMaxTasksPerDay(): number { return this.maxTasksPerDay; }
  setMaxTasksPerDay(_maxTasksPerDay: number) { this.maxTasksPerDay = _maxTasksPerDay; this.isDirty = true; }


  // default image URL/color for task item.
  taskHeaderImage: string;
  taskBackgroundColor: string;


  // get userId(): string { return this._userId; }
  // set userId(_userId: string) { this._userId = _userId; this.isDirty = true; }

  // Note: We use RandomId instead of UniqueId for userId.
  //       This is necessary because userId can be used as Hash Key in DynamoDB.
  constructor(private _uid: (string | null) = null) {
    super(null, _uid);
  }



  toString(): string {
    return super.toString()
      + '; userId = ' + this.userId
      + '; dayHours = ' + this.dayHours
      + '; workHours = ' + this.workHours
      + '; amHours = ' + this.amHours
      + '; pmHours = ' + this.pmHours
      + '; maxTasksPerDay = ' + this.maxTasksPerDay;
  }


  clone(): UserPreference {
    let cloned = Object.assign(new UserPreference(), this) as UserPreference;
    // ???
    // Sometimes, the object loses type information, and .clone() method throws exception...
    // if (this.amHours) cloned.amHours = this.amHours.clone();
    // if (this.pmHours) cloned.pmHours = this.pmHours.clone();
    // if (this.dayHours) cloned.dayHours = this.dayHours.clone();
    // if (this.workHours) cloned.workHours = this.workHours.clone();
    // Instead, just do this ???
    if (this.amHours) cloned.amHours = Object.assign(new HourRange(), this.amHours) as HourRange;
    if (this.pmHours) cloned.pmHours = Object.assign(new HourRange(), this.pmHours) as HourRange;
    if (this.dayHours) cloned.dayHours = Object.assign(new HourRange(), this.dayHours) as HourRange;
    if (this.workHours) cloned.workHours = Object.assign(new HourRange(), this.workHours) as HourRange;
    return cloned;
  }
  // clone(): UserPreference {
  //   return UserPreference.clone(this);
  // }
  static clone(obj: any): UserPreference {
    let cloned = Object.assign(new UserPreference(), obj) as UserPreference;
    if (obj.amHours) cloned.amHours = Object.assign(new HourRange(), obj.amHours) as HourRange;
    if (obj.pmHours) cloned.pmHours = Object.assign(new HourRange(), obj.pmHours) as HourRange;
    if (obj.dayHours) cloned.dayHours = Object.assign(new HourRange(), obj.dayHours) as HourRange;
    if (obj.workHours) cloned.workHours = Object.assign(new HourRange(), obj.workHours) as HourRange;
    return cloned;
  }

  copy(): UserPreference {
    let obj = this.clone();
    // obj.id = RandomIdUtil.id();
    obj.id = UniqueIdUtil.id();
    // userId ????
    obj.resetCreatedTime();
    return obj;
  }
}
